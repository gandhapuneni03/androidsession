package com.example.janeetsuneetha.samplebitbucket;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView customView = (roundedImageView) findViewById(R.id.roundedView);
        customView.setImageResource(R.drawable.default_user);
    }
}
